<?php

//ini_set('memory_limit', '-1');
require('simple_html_dom.php');
date_default_timezone_set('Indean/Maldives');

$backslash = "/";

$botToken="633438220:AAHX83K2pRSXw8vuc4wMTfneBEIu8mvphfA";
$website="https://api.telegram.org/bot".$botToken;

$update = file_get_contents('php://input');
$update = json_decode($update, TRUE);

$chat_id = $update["message"]["chat"]["id"];
$message = $update["message"]["text"];

if(isset($chat_id)){
    
    
    
    switch ($message) {

        case '/today':
            $date = date('dmY');

            $url = "http://www.adkhospital.mv/en/duty/$date/?";

            $html = file_get_html($url);

            $reply = "TODAY'S SCHEDULE\n*******************\n\n";

            foreach ($html->find('tr td[2],tr td[3],tr td[1]') as $tr) {
                if($i==2){
                    $br="\n\n";
                    $i=0;
                }
                else{
                    $br="\n";
                    $i++;
                }
                $reply = $reply . strip_tags($tr) . $br;
            }
            sendMessage($chat_id, $reply);
            break;

        case '/tomorrow':
            $datetime = new DateTime('tomorrow');
            $date = $datetime->format('dmY');

            $url = "http://www.adkhospital.mv/en/duty/$date/?";

            $html = file_get_html($url);

            $reply = "TOMORROW'S SCHEDULE\n*******************\n\n";

            foreach ($html->find('tr td[2],tr td[3],tr td[1]') as $tr) {
                if($i==2){
                    $br="\n\n";
                    $i=0;
                }
                else{
                    $br="\n";
                    $i++;
                }
                $reply = $reply . strip_tags($tr) . $br;
            }
            sendMessage($chat_id, $reply);
            break;

        case '/start':
            if(isset($chat_id)){
                sendMessage($chat_id, "Okay, tell me the name of the doctor. Use one name of the doctor, NOT full name. \n\nFor example: haleema");
            }
            break;


        case '/doctors':
            if(isset($chat_id)){
                sendMessage($chat_id, "Okay, tell me the name of the doctor. Use one name of the doctor, NOT full name. \n\nFor example: haleema");
                
            }
            break;

        default:

            $conn = new mysqli('localhost', 'novits_myadk', 'ahmedshareef.com', 'novits_myadk');
            // Check connection
            if ($conn->connect_error) {
                $reply = "There is a connection error... Please try again later.";
            } 

            $sql = "SELECT `key`,`tags` FROM `synonyms` WHERE `tags` LIKE '%$message%'";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                
                $date = date('dmY');

                // output data of each row
                while($row = $result->fetch_assoc()) {
                    
                    $key = $row["key"];
                    
                    $url = "http://www.adkhospital.mv/en/duty/$date/?&doctor=$key";
                    $html = file_get_html($url);
                    
                    foreach ($html->find('h2,tr td[2],tr td[3],tr td[1]') as $tr) {
                        if($i==3){
                            $br="\n\n";
                            $i=0;
                        }
                        else{
                            $br="\n";
                            $i++;
                        }
                        $reply = $reply . str_replace( "Sorry!", "Sorry! No duty information is currently available for your selection.", strip_tags($tr) ) . $br;
                    }
                }
            } else {
                
                $sql = "INSERT INTO `synonyms` (`key`) VALUES ('$message')";
                $conn->query($sql);

                $reply = "Sorry! I don't understand. I will check this up. \n\nTry a single word. \n\nThanks";
            }

            $conn->close();

            sendMessage($chat_id, $reply);


    }
}

function sendMessage( $chat_id, $reply ){
    $url = $GLOBALS['website']."/sendMessage?chat_id=".$chat_id."&text=".urlencode($reply);
    $url = preg_replace("/ /", "%20", $url);
    file_get_contents($url);
}


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>My ADK Bot</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Overlock:400,900" rel="stylesheet">
    <style>
        body{
            font-family: 'Overlock', cursive;
            color: cadetblue;
        }
        h1,h2,h3,h4,h5,h6{
            font-weight: 900;
            margin-left: 20px;
            color: skyblue;
        }
        p{
            font-weight: 400;
            margin-left: 20px;
        }
        .qrcode{
            width:200px;
            margin:20px;
            float:left;
        }
        .qrcode img{
            width:100%;
        }
        .qrcode div{
            text-transform: uppercase;
            text-align: center;
            margin:0 !important;;
        }
    </style>
</head>
<body>
    <h1>My ADK Bot</h1>
    <p>A telegram bot which can help you with doctor schedule.</p>
    <div class="qrcode">
        <img src="img/self.svg"> 
        <div>Start Now</div>
    </div>
    <div class="qrcode">
        <img src="img/group.svg">
        <div>Add to a group</div>
    </div>
</body>
</html>
